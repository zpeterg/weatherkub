# Test of Kubernetes

A weather app with a front-end and back-end managed by Kubernetes.

## Deploy

1. Bootstrap the master with `bootstrap_master.sh`.
2. Bootstrap the nodes with `boostrap_node.sh` (taking care to replace the join verbage).
3. On the master, create a replication controller (or deployment??), like rc.yml.
4. On the master, deploy it: `kubectl create -f rc.yml`

## Debug

### To get the token for nodes to connect to the master (run on master)

`kubeadm token list`

### To generate a new token (run on master)

`kubeadm token create`

### To find the ca cert hash (run on master)

`openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null | openssl dgst -sha256 -hex | sed 's/^.* //'`
